package com.rackspace;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rzurga on 7/2/16.
 */
class Needle {
    private final String needle;
    private final Map<Character, NeedleChar> needleChars = new HashMap<>();
    private int fullCharsCount = 0;
    private int bestMinPosition = -1;
    private int bestMaxPosition = -1;
    private Integer bestLength = null;

    Needle(String needle) {
        this.needle = needle;
        this.init();
    }

    private void init() {
        for (char c : needle.toCharArray()) {
            NeedleChar needleChar = needleChars.get(c);
            if (needleChar == null) {
                needleChar = new NeedleChar();
                needleChars.put(c, needleChar);
            }
            needleChar.increment();
        }
    }

    public void start() {
        for (NeedleChar needleChar : needleChars.values()) {
            needleChar.start();
        }
    }

    public void nextChar(char c, int position) {
//        System.out.println(String.format("nextChar(%d: %s)", position, c));
        NeedleChar needleChar = needleChars.get(c);
        int minPosition = position;
        int maxPosition = 0;
        if (needleChar != null) {
            needleChar.next(position);
            if (foundSolution()) {
                minPosition = getMinPosition();
                maxPosition = getMaxPosition();
                int length = maxPosition - minPosition + 1;
                if (bestLength == null || length < bestLength) {
                    bestLength = new Integer(length);
                    bestMinPosition = minPosition;
                    bestMaxPosition = maxPosition;
//                    System.out.println(String.format("New best: len/start/stop: %d %d %d", bestLength, bestMinPosition, bestMaxPosition));
                }
            }
        }
    }

    public boolean foundSolution() {
        boolean foundSolution = true;
//        System.out.print("  foundSolution: ");
        for (NeedleChar needleChar : needleChars.values()) {
            foundSolution &= needleChar.isFull();
//            System.out.print(String.format("%b/%d/%d[%s] ", needleChar.isFull(), needleChar.size(), needleChar.getCount(), needleChar.toString()));
        }
//        System.out.println(String.format("=> %b", foundSolution));
        return foundSolution;
    }

    public int[] getSolution() {
        return new int[]{bestMinPosition, bestMaxPosition};
    }

    public int getSolutionLength() {
        return bestLength;
    }

    private int getMinPosition() {
        int minPosition = Integer.MAX_VALUE;
        for (NeedleChar needleChar : needleChars.values()) {
            minPosition = Math.min(minPosition, needleChar.getMinPosition());
        }
        return minPosition;
    }

    private int getMaxPosition() {
        int maxPosition = 0;
        for (NeedleChar needleChar : needleChars.values()) {
            maxPosition = Math.max(maxPosition, needleChar.getMaxPosition());
        }
        return maxPosition;
    }
}
