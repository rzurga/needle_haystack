package com.rackspace;

/**
 * Find shortest needle in a haystack
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: needle_haystack \"needle\" \"haystack\"");
        }
        NeedleInHaystack needleInHaystack = new NeedleInHaystack(args[0], args[1]);
        needleInHaystack.solve();
        boolean foundSolution = needleInHaystack.foundSolution();
        if (foundSolution) {
            int[] solution = needleInHaystack.getSolution();
            int length = needleInHaystack.getSolutionLength();
            System.out.println(String.format("Solution found: [%d, %d] => %d", solution[0], solution[1], length));
        } else {
            System.out.println("Solution not found");
        }
    }
}
