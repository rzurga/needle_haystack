package com.rackspace;

/**
 * Created by rzurga on 7/2/16.
 */
public class NeedleInHaystack {
    private final String needleString;
    private final String haystackString;
    private final Needle needle;

    public NeedleInHaystack(String needle, String haystack) {
        this.needleString = needle;
        this.haystackString = haystack;
        this.needle = new Needle(needleString);
    }
    
    public boolean solve() {
        needle.start();
        int position = 0;
        for (char c: haystackString.toCharArray()) {
            needle.nextChar(c, position++);
        }
        return foundSolution();
    }

    public boolean foundSolution() {
        return needle.foundSolution();
    }

    public int[] getSolution() {
        return needle.getSolution();
    }

    public int getSolutionLength() {
        return needle.getSolutionLength();
    }
}
