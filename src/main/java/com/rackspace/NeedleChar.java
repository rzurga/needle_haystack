package com.rackspace;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rzurga on 7/2/16.
 */
class NeedleChar {
    private int count = 0;
    private Queue<Integer> positions = null;
    private int maxPosition = 0;

    public int increment() {
        return ++count;
    }

    public void start() {
        positions = new LinkedList<>();
    }

    public int next(int position) {
        positions.add(position);
        if (positions.size() > count) {
            positions.remove();
        }
        maxPosition = position;
        return getMinPosition();
    }

    public int getMinPosition() {
        return positions.element();
    }

    public int getMaxPosition() {
        return maxPosition;
    }

    public boolean isFull() {
        return positions.size() == count;
    }

    public int getCount() {
        return count;
    }

    public int size() {
        return positions.size();
    }

    @Override
    public String toString() {
        return positions.toString();
    }
}
