package com.rackspace;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import static org.junit.Assert.assertArrayEquals;


/**
 * Created by rzurga on 7/2/16.
 */
public class NeedleInHaystackTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public NeedleInHaystackTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(NeedleInHaystackTest.class);
    }

    public void testNeedleInHaystack1() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abc", "xyzabcqwe");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{3, 5}, needleInHaystack.getSolution());
        assertEquals(3, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack2() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abc", "xyzcbaqwe");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{3, 5}, needleInHaystack.getSolution());
        assertEquals(3, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack3() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abc", "xyzaqqcbeeecqwe");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{3, 7}, needleInHaystack.getSolution());
        assertEquals(5, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack4() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abc", "xyccczaaaqqcbbbbbeeecqwe");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{8, 12}, needleInHaystack.getSolution());
        assertEquals(5, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack5() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abc", "babbbqqqatc");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{4, 10}, needleInHaystack.getSolution());
        assertEquals(7, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack6() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("abcabc", "aaaaaaaaaabbaqqqacqqqqccccccaaabbbwwwww");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{26, 32}, needleInHaystack.getSolution());
        assertEquals(7, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack7() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("ABC", "xxAxByyyAzzBzCqqq");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{8, 13}, needleInHaystack.getSolution());
        assertEquals(6, needleInHaystack.getSolutionLength());
    }

    public void testNeedleInHaystack8() {
        NeedleInHaystack needleInHaystack = new NeedleInHaystack("ABC", "xxAxByyyBzzAzCqqq");
        needleInHaystack.solve();
        assertTrue(needleInHaystack.foundSolution());
        assertArrayEquals(new int[]{8, 13}, needleInHaystack.getSolution());
        assertEquals(6, needleInHaystack.getSolutionLength());
    }
}
