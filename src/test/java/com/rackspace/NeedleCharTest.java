package com.rackspace;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by robe2589 on 7/2/16.
 */
public class NeedleCharTest extends TestCase {
    public NeedleCharTest(String name) {
        super(name);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(NeedleCharTest.class);
    }


    public void testNeedleChar() {
        NeedleChar needleChar = new NeedleChar();
        needleChar.increment();
        needleChar.increment();
        int count = needleChar.getCount();
        assertEquals(2, count);
        needleChar.start();
        needleChar.next(3);
        assertFalse(needleChar.isFull());
        needleChar.next(5);
        assertTrue(needleChar.isFull());
        needleChar.next(7);
        assertEquals(2, needleChar.size());
        int minPos = needleChar.getMinPosition();
        assertEquals(5, minPos);
        int maxPos = needleChar.getMaxPosition();
        assertEquals(7, maxPos);
    }
}
